local DATA_PATH = vim.fn.stdpath "data"

-- general
lvim.log.level = "warn"
lvim.format_on_save = false
lvim.lint_on_save = true
lvim.colorscheme = "onedarker"
lvim.transparent_window = true
vim.opt.wrap = true
vim.opt.relativenumber = true

-- keymappings
-- keymappings [view all the defaults by pressing <leader>Lk]
lvim.leader = "space"
lvim.keys.normal_mode["X"] = ":BufferClose<CR>"
-- lsp shortcuts
lvim.keys.normal_mode["gd"] = "<cmd>lua vim.lsp.buf.definition()<CR>"
lvim.keys.normal_mode["gD"] = "<cmd>lua vim.lsp.buf.declaration()<CR>"
lvim.keys.normal_mode["gr"] = "<cmd>lua vim.lsp.buf.references()<CR>"
lvim.keys.normal_mode["gi"] = "<cmd>lua vim.lsp.buf.implementation()<CR>"
lvim.keys.normal_mode["gl"] =
  "<cmd>lua vim.lsp.diagnostic.show_line_diagnostics({ show_header = false, border = 'single' })<CR>"
lvim.keys.normal_mode["gs"] = "<cmd>lua vim.lsp.buf.signature_help()<CR>"
lvim.keys.normal_mode["gp"] = "<cmd>lua require'lsp.peek'.Peek('definition')<CR>"
lvim.keys.normal_mode["K"] = "<cmd>lua vim.lsp.buf.hover()<CR>"
-- skip typescript hints when cycling diagnostics
lvim.keys.normal_mode["<C-p>"] =
  '<cmd>lua vim.lsp.diagnostic.goto_prev({popup_opts = {border = lvim.lsp.popup_border}, severity_limit = vim.bo.filetype == \'typescriptreact\' and "Information" or "Hint"})<CR>'
lvim.keys.normal_mode["<C-n>"] =
  '<cmd>lua vim.lsp.diagnostic.goto_next({popup_opts = {border = lvim.lsp.popup_border}, severity_limit = vim.bo.filetype == \'typescriptreact\' and "Information" or "Hint"})<CR>'

-- Change Telescope navigation to use j and k for navigation and n and p for history in both input and normal mode.
-- we use protected-mode (pcall) just in case the plugin wasn't loaded yet.
local _, actions = pcall(require, "telescope.actions")
lvim.builtin.telescope.defaults.mappings = {
  -- for input mode
  i = {
    ["<C-j>"] = actions.move_selection_next,
    ["<C-k>"] = actions.move_selection_previous,
    ["<C-n>"] = actions.cycle_history_next,
    ["<C-p>"] = actions.cycle_history_prev,
  },
  -- for normal mode
  n = {
    ["<C-j>"] = actions.move_selection_next,
    ["<C-k>"] = actions.move_selection_previous,
  },
}

-- Use which-key to add extra bindings with the leader-key prefix
lvim.builtin.which_key.mappings["P"] = { "<cmd>Telescope projects<CR>", "Projects" }
lvim.builtin.which_key.mappings["t"] = {
  name = "+Trouble",
  r = { "<cmd>Trouble lsp_references<cr>", "References" },
  f = { "<cmd>Trouble lsp_definitions<cr>", "Definitions" },
  d = { "<cmd>Trouble lsp_document_diagnostics<cr>", "Diagnostics" },
  q = { "<cmd>Trouble quickfix<cr>", "QuickFix" },
  l = { "<cmd>Trouble loclist<cr>", "LocationList" },
  w = { "<cmd>Trouble lsp_workspace_diagnostics<cr>", "Diagnostics" },
}
lvim.builtin.which_key.mappings["r"] = { "<cmd>RnvimrToggle<cr>", "Ranger" }

-- TODO: User Config for predefined plugins
-- After changing plugin config exit and reopen LunarVim, Run :PackerInstall :PackerCompile
lvim.builtin.dashboard.active = true
lvim.builtin.terminal.active = true
lvim.builtin.nvimtree.setup.view.side = "left"
lvim.builtin.nvimtree.show_icons.git = 0
lvim.builtin.dashboard.active = true
lvim.builtin.treesitter.ensure_installed = {
  "bash",
  "javascript",
  "json",
  "lua",
  "typescript",
  "css",
  "yaml",
}
lvim.builtin.treesitter.ignore_install = { "haskell" }
lvim.builtin.treesitter.highlight.enabled = true

-- generic LSP settings

-- ---@usage disable automatic installation of servers
-- lvim.lsp.automatic_servers_installation = false

-- ---@usage Select which servers should be configured manually. Requires `:LvimCacheRest` to take effect.
-- See the full default list `:lua print(vim.inspect(lvim.lsp.override))`
vim.list_extend(lvim.lsp.override, { "tsserver", "intelephense", "texlab" })

-- ---@usage setup a server -- see: https://www.lunarvim.org/languages/#overriding-the-default-configuration
require("lvim.lsp.manager").setup("tsserver", {
  cmd = {
    DATA_PATH .. "/lspinstall/typescript/node_modules/.bin/typescript-language-server",
    "--stdio",
    "--tsserver-path=/home/dklymenk/Documents/personal/TypeScript/lib/tsserver.js",
  },
  root_dir = require("lspconfig").util.root_pattern("Makefile", ".git", "node_modules"),
  handlers = { ["textDocument/publishDiagnostics"] = function() end },
})
require("lvim.lsp.manager").setup("texlab", {
  settings = {
    texlab = {
      chktex = {
        onEdit = true,
        onOpenAndSave = true,
      },
      build = {
        args = { "%f" },
        executable = "pdflatex",
        onSave = true,
      },
    },
  },
})
require("lvim.lsp.manager").setup("intelephense", {
  settings = {
    intelephense = {
      environment = { phpVersion = "7.4" },
      files = {
        associations = {
          "*.php",
          "*.phtml",
          "*.inc",
          "*.module",
          "*.install",
          "*.theme",
          ".engine",
          ".profile",
          ".info",
          ".test",
        },
      },
    },
  },
  cmd = {
    DATA_PATH .. "/lspinstall/php/node_modules/.bin/intelephense",
    "--stdio",
  },
})
require("lspconfig")["phpactor"].setup {
  handlers = {
    ["textDocument/hover"] = function() end,
  },
}

-- you can set a custom on_attach function that will be used for all the language servers
-- See <https://github.com/neovim/nvim-lspconfig#keybindings-and-completion>
-- lvim.lsp.on_attach_callback = function(client, bufnr)
--   local function buf_set_option(...)
--     vim.api.nvim_buf_set_option(bufnr, ...)
--   end
--   --Enable completion triggered by <c-x><c-o>
--   buf_set_option("omnifunc", "v:lua.vim.lsp.omnifunc")
-- end

-- add node_modules to null_ls root pattern
lvim.lsp.null_ls.setup = {
  root_dir = require("lspconfig").util.root_pattern("Makefile", ".git", "node_modules"),
}

-- -- set a formatter, this will override the language server formatting capabilities (if it exists)
local formatters = require "lvim.lsp.null-ls.formatters"
formatters.setup {
  {
    exe = "eslint_d",
    filetypes = { "javascript" },
  },
  {
    exe = "stylua",
    filetypes = { "lua" },
  },
}

-- -- set additional linters
local linters = require "lvim.lsp.null-ls.linters"
linters.setup {
  {
    exe = "eslint_d",
    filetypes = { "javascript" },
  },
}

-- Additional Plugins
lvim.plugins = {
  { "kevinhwang91/rnvimr" },
  { "iamcco/markdown-preview.nvim", run = "cd app && yarn install" },
  { "mattn/emmet-vim" },
  {
    "norcalli/nvim-colorizer.lua",
    config = function()
      require("colorizer").setup({ "*" }, {
        RGB = true, -- #RGB hex codes
        RRGGBB = true, -- #RRGGBB hex codes
        RRGGBBAA = true, -- #RRGGBBAA hex codes
        rgb_fn = true, -- CSS rgb() and rgba() functions
        hsl_fn = true, -- CSS hsl() and hsla() functions
        css = true, -- Enable all CSS features: rgb_fn, hsl_fn, names, RGB, RRGGBB
        css_fn = true, -- Enable all CSS *functions*: rgb_fn, hsl_fn
      })
    end,
  },
  { "ruanyl/vim-gh-line" },
  {
    "folke/trouble.nvim",
    cmd = "TroubleToggle",
  },
}

-- Autocommands (https://neovim.io/doc/user/autocmd.html)
lvim.autocommands.custom_groups = {
  -- select indentation by language
  -- { "FileType", "php", "setlocal ts=2 sw=2 sts=2" },
  { "FileType", "php", "setlocal ts=4 sw=4 sts=4" },
  { "FileType", "javascript", "setlocal ts=2 sw=2 sts=2" },
  -- { "FileType", "javascript", "setlocal ts=4 sw=4 sts=4" },
  -- disable useless typescript hints
  -- { "FileType", "typescriptreact" , "lua lvim.lsp.diagnostics.virtual_text.severity_limit = \"Information\""},
  -- { "FileType", "typescriptreact" , "lua lvim.lsp.diagnostics.signs.severity_limit = \"Information\""},
  -- { "FileType", "typescriptreact" , "lua lvim.lsp.diagnostics.underline = {severity_limit = \"Information\"}"},
}

-- Load custom snippets
require("luasnip/loaders/from_vscode").lazy_load { paths = "~/.config/lvim/snippets" }
